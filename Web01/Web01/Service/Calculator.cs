﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web01.Service
{
    public class Calculator
    {
        public double Plus(double number1, double number2)
        {
            return number1 + number2;
        }
    }
}
