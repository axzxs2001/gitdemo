﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web01.Service
{
    public interface ICalculator
    {
        double Plus(double number1, double number2);
    }
}
